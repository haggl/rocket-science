# Rocket-science
A collection of nice animations for CCC's [blinkenrocket](http://blinkenrocket.de).

## Usage
Let's say your favorite browser is chromium and you want to load the animation `wave`:
```sh
chromium `cat wave`
```
As simple as that.